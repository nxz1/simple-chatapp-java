variable "ecs_cluster_name" {}
variable "container_insights" {}
variable "ecs_capacity_provider_name" {}
variable "managed_termination_protection" {}
variable "task_def_family_name" {}
variable "container_definitions_path" {}
variable "ecs_service_name" {}
variable "cw_log_group_name" {}
variable "ecs_managed_scaling_status" {}
variable "asg_arn" {}
variable "target_group_arn" {}
variable "web_listener_group" {}
variable "listener_arn" {}
variable "environment_name" {}
variable "container_name" {}

variable "container_port" {
    type = number
}