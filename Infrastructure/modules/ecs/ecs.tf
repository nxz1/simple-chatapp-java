resource "aws_ecs_cluster" "my_cluster" {
  name = "${var.environment_name}-${var.ecs_cluster_name}"

  setting {
    name  = "containerInsights"
    value = "${var.container_insights}"
  }
}

resource "aws_ecs_capacity_provider" "my_capacity_provider" {
  name = "ASG"
  auto_scaling_group_provider {
    auto_scaling_group_arn         = "${var.asg_arn}"
    managed_termination_protection = "${var.managed_termination_protection}"

    managed_scaling {
      maximum_scaling_step_size = 100
      minimum_scaling_step_size = 1
      status          = "${var.ecs_managed_scaling_status}"
      target_capacity = 60
    }
  }
}

resource "aws_ecs_task_definition" "my_task_definition" {
  family                = "${var.task_def_family_name}"
  container_definitions = file("${var.container_definitions_path}")
  network_mode          = "bridge"
  tags = {
    "env"       = "dev"
    "createdBy" = "mkerimova"
  }
}

data "aws_lb_listener" "web_listener" {
  arn = var.listener_arn
}

resource "aws_ecs_service" "service" {
  name            = "${var.ecs_service_name}"
  cluster         = aws_ecs_cluster.my_cluster.id
  task_definition = aws_ecs_task_definition.my_task_definition.arn
  desired_count   = 4
  ordered_placement_strategy {
    type  = "spread"
    field = "host"
  }
  load_balancer {
    target_group_arn = "${var.target_group_arn}"
    container_name   = "${var.container_name}"
    container_port   = "${var.container_port}"
  }
  # Optional: Allow external changes without Terraform plan difference(for example ASG)
  lifecycle {
    ignore_changes = [desired_count]
  }
  launch_type = "EC2"
  depends_on = [data.aws_lb_listener.web_listener]
}

resource "aws_cloudwatch_log_group" "log_group" {
  name = "${var.cw_log_group_name}"
  tags = {
    "env"       = "${var.environment_name}-cw-group"
  }
}