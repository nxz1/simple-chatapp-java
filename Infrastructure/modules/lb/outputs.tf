output "dns_name" {
    value = "${aws_lb.my_lb.dns_name}"
}

output "target_group_arn" {
    value = "${aws_lb_target_group.lb_target_group.arn}"
}

output "web_listener_group" {
    value = "${aws_lb_listener.web_listener}"
}

output "web_listener_arn" {
    value = "${aws_lb_listener.web_listener.arn}"
}