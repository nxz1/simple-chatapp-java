output "ecs_instance_iam_service_role" {
    value = "${aws_iam_instance_profile.ecs_service_role.name}"
}