# AWS Config
aws_region = "<YOUR AWS PROFILE REGION HERE>"
aws_profile = "<YOUR AWS PROFILE HERE>"
environment_name = "dev"    

# VPC
vpc_cidr = "192.168.0.0/16"
# To configure subnets go to the module and modify public_subnets and private_subnets resources.

# ASG
ecs_instance_sg_name = "portfolio-allow-all-inbound-ec2"
ecs_instance_lc_name = "portfolio-t2-medium-lc"
ecs_instance_key_name = "<YOUR KEY HERE>"

# ECS Cluster
ecs_cluster_name = "<YOUR CLUSTER NAME>"
container_insights = "enabled"
ecs_managed_scaling_status = "ENABLED"
termination_protection = "ENABLED"
ecs_capacity_provider_name = "custom_capacity_provider"
container_definitions_path = "container-definitions/portfolio.json"
task_def_family_name = "<YOUR SERVICE FAMILY>"

# ECS Service
ecs_service_name = "<YOUR SERVICE NAME>"
cw_log_group_name = "/ecs/my-ecs-cluster-logging"
service_container_name = "blue-portfolio"
service_container_port = 80

# ELB
lb_name = "<YOUR SERVICE NAME>-lb"
lb_sg_name = "<YOUR SERVICE NAME>-lb-sg"
lb_tg_name = "<YOUR SERVICE NAME>-tg-HTTP"

# IAM
ecs_instance_role_name = "ecsInstanceRole"
