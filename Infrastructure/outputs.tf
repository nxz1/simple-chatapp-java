output "alb_dns" {
  value = module.alb.dns_name
}

output "vpc_id" {
  value = module.networking.vpc_id
}

output "public_subnets" {
  value = module.networking.public_subnets
}

output "private_subnets" {
  value = module.networking.private_subnets
}

output "igw_id" {
  value = module.networking.igw_id
}