provider "aws" {
    region = "${var.aws_region}"
    profile = "${var.aws_profile}"
}

 terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

module "networking" {
    source = "./modules/networking"
    name = "${var.environment_name}-${var.environment_name}"
    vpc_cidr = [ "${var.vpc_cidr}" ]
}

module "iam" {
    source = "./modules/iam"
    ecs_instance_role_name = "${var.ecs_instance_role_name}"
}

module "ecs_cluster" {
    source = "./modules/ecs"
    environment_name = "${var.environment_name}"
    ecs_cluster_name = "${var.ecs_cluster_name}"
    container_insights = "${var.container_insights}"
    ecs_managed_scaling_status = "${var.ecs_managed_scaling_status}"
    managed_termination_protection = "${var.termination_protection}"
    
    # Task Definitions
    container_definitions_path = "${var.container_definitions_path}"
    task_def_family_name = "${var.task_def_family_name}"
    
    # Service
    ecs_service_name = "${var.ecs_service_name}"
    asg_arn = "${module.asg.asg_arn}"
    target_group_arn = "${module.alb.target_group_arn}"
    web_listener_group = "${module.alb.web_listener_group}"
    container_name = "${var.service_container_name}"
    container_port = "${var.service_container_port}"
    
    # Capacity Provider
    ecs_capacity_provider_name = "${var.ecs_capacity_provider_name}"
    listener_arn = "${module.alb.web_listener_arn}"
    cw_log_group_name = "${var.cw_log_group_name}"    
}

module "asg" {
    source = "./modules/asg"
    environment_name = "${var.environment_name}"
    ecs_instance_sg_name = "${var.ecs_instance_sg_name}"
    ecs_instance_lc_name = "${var.ecs_instance_lc_name}"
    ecs_instance_key_name = "${var.ecs_instance_key_name}"
    ecs_cluster_name = "${var.ecs_cluster_name}"
    ec2_vpc_id = "${module.networking.vpc_id}"
    vpc_zone_identifier = "${module.networking.public_subnets}"
    iam_instance_profile = "${module.iam.ecs_instance_iam_service_role}"
    target_group_arn = "${module.alb.target_group_arn}"
}

module "alb" {
    source = "./modules/lb"
    loadbalancer_name = "${var.environment_name}-${var.lb_name}"
    loadbalancer_sg_name = "${var.environment_name}-${var.lb_sg_name}"
    loadbalancer_tg = "${var.lb_tg_name}"
    vpc_id = "${module.networking.vpc_id}"
    subnets = "${module.networking.public_subnets}"
    
}