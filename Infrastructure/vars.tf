
variable "aws_region" {}
variable "aws_profile" {}

# VPC
variable "environment_name" {}
variable "vpc_cidr" {}

# IAM
variable "ecs_instance_role_name" {}

# ASG
variable "ecs_instance_sg_name" {}
variable "ecs_instance_lc_name" {}
variable "ecs_instance_key_name" {}

# ECS 
variable "ecs_cluster_name" {}
variable "container_insights" {}
variable "container_definitions_path" {}
variable "ecs_managed_scaling_status" {}
variable "termination_protection" {}
variable "ecs_capacity_provider_name" {}
variable "ecs_service_name" {}
variable "cw_log_group_name" {}
variable "task_def_family_name" {}

variable "service_container_name" {}
variable "service_container_port" {
    type = number
}

# ALB
variable "lb_tg_name" {}
variable "lb_sg_name" {}
variable "lb_name" {}