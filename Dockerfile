FROM maven:3-jdk-8-alpine AS build
LABEL Maintainer="Mohammed Nawaz"
WORKDIR /usr/src/build
COPY ./Code .
RUN mvn clean package

FROM openjdk:18-alpine3.15
LABEL Maintainer="Mohammed Nawaz"
ENV PORT=9090
ARG env="develop"
WORKDIR /usr/src/app
COPY --from=build /usr/src/build/target/chatapp.jar .
EXPOSE $PORT
CMD [ "java", "-jar", "chatapp.jar"]
